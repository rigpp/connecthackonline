﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerbulletmaker : MonoBehaviour {

    public Animator anim;
    public Transform[] danmaku=new Transform[16];
    int i = 0;
    public GameObject bullet;

    public float second = 0.1f;
    public GameObject tama;
    public Transform a;

	// Use this for initialization
	void Start () {

        StartCoroutine(bulletmake());
        anim.SetBool("kaiten", true);


	}
	
	// Update is called once per frame
	void Update () {

        if(Input.GetMouseButtonDown(0) && YuusyaGenerator1.gage >= 14)
        { 
            Instantiate(tama,a.transform.position,a.transform.rotation);
            YuusyaGenerator1.gage = 0;
        }

	}

    IEnumerator bulletmake()
    {
        while (true)
        {
            for (i = 0; i < danmaku.Length; i++)
            {
                Instantiate(bullet, danmaku[i].position, danmaku[i].rotation);

                yield return new WaitForSeconds(0.01f);
            }

          //  yield return new WaitForSeconds(second);
        }
       
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "enemy")
        {
            AIforwardmove.a -= 20;
        }
    }
}
