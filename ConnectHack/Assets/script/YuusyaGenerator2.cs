﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YuusyaGenerator2 : MonoBehaviour {
    public GameObject Yuusya;
    public GameObject Zako1;
    public GameObject Zako2;
    public GameObject Zako3;
    public int YuusyaNumber = 0;
    int i;

    // Use this for initialization
    void Start()
    {
        for (int Ynumber = 1; Ynumber <= 10; Ynumber++)
        {
            i = Random.Range(1, 50);
            if (i == 1) {
                Instantiate(Yuusya, new Vector3(0, 0, 0), Quaternion.identity);
            } else if (1 < i && i <= 17) {
                Instantiate(Zako1, new Vector3(0, 0, 0), Quaternion.identity);
            } else if (17 < i && i <= 33) {
                Instantiate(Zako2, new Vector3(0, 0, 0), Quaternion.identity);
            } else if (33 < i && i <= 49) {
                Instantiate(Zako3, new Vector3(0, 0, 0), Quaternion.identity);
            }
            YuusyaNumber ++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (YuusyaNumber <= 10)
        {
            i = Random.Range(1, 50);
            if (i == 1)
            {
                Instantiate(Yuusya, new Vector3(0, 0, 0), Quaternion.identity);
            }
            else if (1 < i && i <= 17)
            {
                Instantiate(Zako1, new Vector3(0, 0, 0), Quaternion.identity);
            }
            else if (17 < i && i <= 33)
            {
                Instantiate(Zako2, new Vector3(0, 0, 0), Quaternion.identity);
            }
            else if (33 < i && i <= 49)
            {
                Instantiate(Zako3, new Vector3(0, 0, 0), Quaternion.identity);
            }
            YuusyaNumber++;
        }

    }
}

