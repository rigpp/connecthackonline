﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class forwardmove : MonoBehaviour {

    RaycastHit hit;

    [SerializeField]
    bool isEnable = true;

    Rigidbody rib;

    public  float movespeedplayer=100;

    public float raydis=30,spheredis=10;

    Vector3 hitpoint;

    public bool move;

    
	// Use this for initialization
	void Start () {
        rib = GetComponent<Rigidbody>();
       



	}
	
	// Update is called once per frame
	void Update () {
        aaa();
        rib.velocity = transform.TransformDirection(Vector3.forward) * movespeedplayer;
    }

   

    void aaa()
    {
        if (isEnable == false)
            return;

       
        

        if (Physics.SphereCast(transform.position, spheredis, transform.TransformDirection(Vector3.forward) , out hit, raydis))
        {

           // Gizmos.DrawRay(transform.position, transform.forward * hit.distance);
            //Gizmos.DrawWireSphere(transform.position + transform.forward * (hit.distance), spheredis);
       
          

            hitpoint = hit.point;
            if (hit.collider.gameObject.tag=="enemy")
            {
                AIforwardmove tmp = hit.collider.gameObject.GetComponent<AIforwardmove>();
                tmp.attack = hitpoint;
                tmp.time = 0.01f;
                move = true;
            }
            else
            {

                move = false;
            }

        }
        else
        {
          //  Gizmos.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 100);

            hitpoint = transform.position + transform.TransformDirection(Vector3.forward) * 20;
            


        }



    }
}
